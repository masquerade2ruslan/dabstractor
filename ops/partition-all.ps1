﻿Import-Module NetSecurity

$Username = 'student'
$Password = 'Utmti2004'

$N1 = "192.168.160.128"
$N2 = "192.168.160.129"
$N3 = "192.168.160.130"

$pass = ConvertTo-SecureString -AsPlainText $Password -Force
$Cred = New-Object System.Management.Automation.PSCredential -ArgumentList $Username,$pass

function CreateFirewallRules () {
    
    Set-NetFirewallRule -DisplayName “Allow Inbound Port 1234”  -Enabled False  
    Set-NetFirewallRule -DisplayName "Block Outbound Port 1234" -Enabled True
}

Invoke-Command -Credential $Cred -ComputerName $N1 -ScriptBlock ${function:CreateFirewallRules}
Invoke-Command -Credential $Cred -ComputerName $N2 -ScriptBlock ${function:CreateFirewallRules}
Invoke-Command -Credential $Cred -ComputerName $N3 -ScriptBlock ${function:CreateFirewallRules}