﻿#setup collection of IP address strings, include original IP
$ips = 115..120 | %{ "192.168.1.$_" }

#also subnet masks, need as many as IP addresses
$masks = 115..120 | %{ "255.255.255.0" }

#get the NIC
$nic = Get-WMIObject win32_networkadapterconfiguration | where { $_.IPAddress -contains "192.168.1.115" }

#set addresses
$nic.EnableStatic($ips,$masks)
$nic.SetGateways("192.168.1.1")
$nic.SetDNSServerSearchOrder("8.8.8.8")