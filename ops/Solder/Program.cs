﻿using System;

namespace Solder
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Please start again with 2 args. Ex:  10.10.10.1 10.10.10.2,10.10.10.3' ");

                Console.ReadKey();
                Environment.Exit(0);
            }

            string host = args[0];
            string[] peers = args[1].Split(',');

            new Vnode(host, peers);

            Console.ReadKey();
        }
    }
}
