using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Solder
{
    public class Vnode
    {
        private readonly string _host;
        private readonly string[] _peers;

        public Vnode(string host, string[] peers)
        {
            _host = host;
            _peers = peers;

            Task.Factory.StartNew(() => InLoop(host));
            Task.Factory.StartNew(() => OutLoop(peers));
        }

        private void OutLoop(string[] peers)
        {
            while (true)
            {
                foreach (var peer in peers)
                {
                    try
                    {
                        using (var client = new TcpClient())
                        {
                            if (!client.ConnectAsync(peer, 1234).Wait(1000))
                            {
                                Print("\\> unreachble:" + peer);
                                continue;
                            }

                            using (var stream = client.GetStream())
                            {
                                using (var reader = new StreamReader(stream))
                                using (var writer = new StreamWriter(stream) { AutoFlush = true })
                                {
                                    writer.WriteLine("ping");
                                    Print("\\> rep:" + reader.ReadLine());
                                }
                            }
                        }
                    }
                    catch (SocketException e)
                    {
                        Print("\\> unreachble:" + peer);
                    }

                    Thread.Sleep(1000);
                }
            }
        }

        private void InLoop(string host)
        {
            Print("\\> started");

            var listener = new TcpListener(IPAddress.Parse(host), 1234);
            listener.Start();

            while (true)
            {
                using (var client = listener.AcceptTcpClient())
                using (var stream = client.GetStream())
                using (var reader = new StreamReader(stream))
                using (var writer = new StreamWriter(stream) { AutoFlush = true })
                {
                    Print("\\> req:" + reader.ReadLine());

                    writer.WriteLine("pong");
                }
            }
        }

        private void Print(string text)
        {
            Console.WriteLine(_host + " " + text);
        }
    }
}