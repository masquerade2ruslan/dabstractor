﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Dabstractor.P2pLinks;
using NLog;

namespace Dabstractor.FailureDetectors
{
    public delegate void CrashedHandler(string peer);

    public class PerfectFailureDetector
    {
        private static readonly Logger Logger = LogManager.GetLogger("pfd");
        readonly CancellationTokenSource _ts = new CancellationTokenSource();

        readonly Pipeline<TransportMessage> _pipeline = new Pipeline<TransportMessage>();

        private readonly string _host;
        private readonly List<string> _peers;
        private readonly List<string> _alive = new List<string>();
        private readonly List<string> _detected = new List<string>();
        readonly PerfectLink _pl;

        public event CrashedHandler Crash;


        public PerfectFailureDetector(string host, List<string> peers)
        {
            _host = host;
            _peers = peers;

            _peers.ToList().Remove(_host);

            _pl = new PerfectLink(_host);
            _pl.Deliver += Inbound;

            _pipeline.Timeout += Pipeline_Timeout;
            _pipeline.RequestTimeout(2000, DateTime.Now);
        }

        private void Pipeline_Timeout(object state)
        {
            if (_ts.Token.IsCancellationRequested)
                return;

            if (state is DateTime)
            {
                _alive.Clear();
                _pipeline.RequestTimeout(2000, DateTime.Now);

                Logger.Info(_host + " Evaluate Failed nodes");

                foreach (var peer in _peers)
                {
                    _pl.Send(new TransportMessage(peer, "HartbeatRequest"));
                    _pipeline.RequestTimeout(1500, peer);
                }  
            }

            if (state is string)
            {
                var peer = (string) state;

                if (_alive.Contains(peer) == false && _detected.Contains(peer) == false)
                {
                    _detected.Add(peer);

                    Logger.Info(_host + " detected as crashed " + peer);

                    Logger.Info(_host + " Alive    " + string.Join(" ", _alive.ToArray()));
                    Logger.Info(_host + " Detected " + string.Join(" ", _detected.ToArray()));
                    Logger.Info(_host + " Peers    " + string.Join(" ", _peers.ToArray()));

                    if (Crash != null)
                        Crash(peer);
                }
            }
        }

        private void Inbound(TransportMessage message)
        {
            if (message.Payload == "HartbeatRequest")
            {
                _pl.Send(new TransportMessage(message.From, "HartbeatReply"));
                return;
            }

            if (message.Payload == "HartbeatReply")
            {
                _alive.Add(message.From);
                Logger.Info(_host + " Healthy " + message.From);
            }
        }

        public void Dispose()
        {
            _pl.Dispose();
            _ts.Cancel();

            Logger.Info(_host + " disposed");
        }
    }
}