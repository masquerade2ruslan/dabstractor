﻿using System;
using System.Collections.Generic;
using Dabstractor.P2pLinks;
using NLog;

namespace Dabstractor.Broadcasts
{
    public class BestEffortBroadcast : IDisposable
    {
        private static readonly Logger Logger = LogManager.GetLogger("beb");

        private readonly string _host;
        private readonly List<string> _peers;

        private readonly PerfectLink _pl;
        readonly Pipeline<TransportMessage> _pipeline = new Pipeline<TransportMessage>();

        public event DeliveryHandler Deliver;

        public BestEffortBroadcast(string host, List<string> peers)
        {
            _host = host;
            _peers = peers;

            _pl = new PerfectLink(host);
            _pl.Deliver += Inbound;

            _pipeline.Dispatch += OnDispatchMessage;
        }

        public void Broadcast(TransportMessage message)
        {
            Logger.Info(_host + " broadcast " + message.Payload);

            foreach (var peer in _peers)
            {
                _pl.Send(message.Clone(m =>
                {
                    m.Id = Guids.GenerateComb();
                    m.To = peer;
                }));
            }
        }

        private void Inbound(TransportMessage message)
        {
            _pipeline.Send(message);
        }

        private void OnDispatchMessage(TransportMessage message)
        {
            if (Deliver != null)
            {
                Logger.Info(_host + " broadcast deliver " + message.Payload);
                Deliver(message);
            }
        }

        public void Dispose()
        {
            _pl.Dispose();
            _pipeline.Dispose();

            Logger.Info(_host + " disposed");
        }
    }
}