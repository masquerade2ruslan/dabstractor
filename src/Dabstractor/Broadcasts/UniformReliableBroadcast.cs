using System;
using System.Collections.Generic;
using Dabstractor.P2pLinks;
using NLog;

namespace Dabstractor.Broadcasts
{
    public class UniformReliableBroadcast : IDisposable
    {
        private static readonly Logger Logger = LogManager.GetLogger("urb");

        private readonly string _host;
        private readonly List<string> _peers;

        private readonly BestEffortBroadcast _beb;
        readonly Pipeline<TransportMessage> _pipeline = new Pipeline<TransportMessage>();

        public event DeliveryHandler Deliver;

        private Dictionary<string, TransportMessage> _pending = new Dictionary<string, TransportMessage>(); 
        private Dictionary<string, List<string>> _ack = new Dictionary<string, List<string>>(); 
        private List<string> _delivered = new List<string>(); 

        public UniformReliableBroadcast(string host, List<string> peers)
        {
            _host = host;
            _peers = peers;

            _beb = new BestEffortBroadcast(host,_peers);
            _beb.Deliver += BebDeliver;

            _pipeline.Dispatch += OnDispatchMessage;
        }

        public void Broadcast(TransportMessage message)
        {
            Logger.Info(_host + " broadcast " + message.Payload);

            message.Headers["data"] = message.Id;

            _pending[message.Id] = message;

            _beb.Broadcast(message);
        }

        private void BebDeliver(TransportMessage message)
        {
            _pipeline.Send(message);
        }

        private void OnDispatchMessage(TransportMessage message)
        {
            var data = message.Headers["data"];

            if(!_ack.ContainsKey(data))
                _ack[data] = new List<string>();

            _ack[data].Add(message.From);

            if (!_pending.ContainsKey(data))
            {
                Logger.Info(_host + " pending " + data);
                _pending[data] = message;
                _beb.Broadcast(message);
            }

            if (_pending.ContainsKey(data) && 
                (_ack[data].Count > (_peers.Count/2)) &&
                _delivered.Contains(data) == false)
            {
                _delivered.Add(message.Id);

                if (Deliver != null)
                {
                    Logger.Info(_host + " broadcast deliver " + message.Payload);
                    Deliver(message);
                }
            }
        }

        public void Dispose()
        {
            _beb.Dispose();
            _pipeline.Dispose();

            Logger.Info(_host + " disposed");
        }
    }
}