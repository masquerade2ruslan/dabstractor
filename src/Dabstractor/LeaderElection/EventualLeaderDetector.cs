﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dabstractor.P2pLinks;
using Dabstractor.Storage;
using NLog;

namespace Dabstractor.LeaderElection
{
    public class EventualLeaderDetector : IDisposable
    {
        private static readonly Logger Logger = LogManager.GetLogger("eld");

        private readonly string _host;
        public string CurrentLeader;

        private readonly List<string> _members;
        private readonly Dictionary<string,long> _candidates = new Dictionary<string, long>();
        private readonly Checkpoint _checkpoint;

        private readonly long _epoch = 0;

        readonly Pipeline<TransportMessage> _pipeline = new Pipeline<TransportMessage>();

        readonly FairLossLink _fll;

        public EventualLeaderDetector(string host, List<string> members)
        {
            _host = host;
            _members = members;

            _fll = new FairLossLink(_host);
            _fll.Deliver += Inbound;

            _checkpoint = new Checkpoint(new FileInfo(host.Replace(".","") + ".cp"));
            
            _epoch = _checkpoint.GetOrInitPosition() + 1;
            _checkpoint.Update(_epoch);

            _pipeline.Timeout += Pipeline_Timeout;
            _pipeline.RequestTimeout(500, DateTime.Now);

            foreach (var member in members)
                _fll.Send(new TransportMessage(member, _epoch.ToString()));

            Logger.Info(_host + " started");
        }

        private void Inbound(TransportMessage message)
        {
            long ep = 0;
            if (long.TryParse(message.Payload, out ep))
            {
                Logger.Debug(_host + " HEARTBEAT " + message.From  + "  " + message.Payload);
                _candidates[message.From] = ep;
            }
        }

        private void Pipeline_Timeout(object message)
        {
            var newLeader = SelectLeader();

            if (newLeader != CurrentLeader)
            {
                Logger.Debug(_host + " SELECTED Leader " + newLeader);
                CurrentLeader = newLeader;
            }

            _candidates.Clear();

            foreach (var member in _members)
                _fll.Send(new TransportMessage(member, _epoch.ToString()));

            _pipeline.RequestTimeout(500, DateTime.Now);
        }

        private string SelectLeader()
        {
            var leader = _candidates.OrderBy(x=>x.Value).FirstOrDefault();

            if (!leader.Equals(default(KeyValuePair<string, long>)))
                return leader.Key;

            return "Failed";
        }

        public void Dispose()
        {
            _checkpoint.Dispose();
            _fll.Dispose();
            _pipeline.Dispose();

            Logger.Info(_host + " disposed");
        }
    }
}