﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dabstractor.FailureDetectors;
using NLog;

namespace Dabstractor.LeaderElection
{
    public delegate void LeaderHandler(string peer);

    public class MonarchicalLeaderElection : IDisposable
    {
        private static readonly Logger Logger = LogManager.GetLogger("mle");

        private readonly string _host;

        public string CurrentLeader;
        public event LeaderHandler Leader;

        private readonly List<string> _members;
        private readonly List<string> _suspected = new List<string>();

        private readonly PerfectFailureDetector _pfd;

        public MonarchicalLeaderElection(string host, List<string> members)
        {
            _host = host;
            _members = members;

            _pfd = new PerfectFailureDetector(host, members);
            _pfd.Crash += WhenCrash;

            CurrentLeader = RankByMaxLastIpDigit(Alive());

            Logger.Info(_host + " started");
        }

        private string RankByMaxLastIpDigit(List<string> alive)
        {
            var maxRank = string.Empty;
            var maxRankDigit = 0;

            foreach (var peer in alive)
            {
                var peerDigit = int.Parse(peer.Split(',')[0].Split('.')[3]);

                if (maxRankDigit < peerDigit)
                {
                    maxRankDigit = peerDigit;
                    maxRank = peer;
                }
            }

            if (Leader != null)
                Leader(maxRank);

            return maxRank;
        }

        private void WhenCrash(string peer)
        {
            if(!_suspected.Contains(peer))
                _suspected.Add(peer);

            CurrentLeader = RankByMaxLastIpDigit(Alive());
        }

        private List<string> Alive()
        {
            return _members.Where(m => !_suspected.Contains(m)).ToList();
        }

        public void Dispose()
        {
            _pfd.Dispose();
            Logger.Info(_host + " disposed");
        }
    }
}