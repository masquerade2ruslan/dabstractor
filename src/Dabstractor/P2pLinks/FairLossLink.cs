﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NLog;

namespace Dabstractor.P2pLinks
{
    public class FairLossLink : IDisposable
    {
        private readonly string _host;
        private readonly TcpListener _listener;


        private readonly Logger _logger = LogManager.GetLogger("f");

        readonly Pipeline<TransportMessage> _pipeline = new Pipeline<TransportMessage>();
        readonly TransportMessageSerializer _serializer = new TransportMessageSerializer();
        readonly CancellationTokenSource _ts = new CancellationTokenSource();
        readonly object _lockObject = new object();

        public readonly Dictionary<string, TcpClient> Destinations = new Dictionary<string, TcpClient>();
        public event DeliveryHandler Deliver;

        public FairLossLink(string host)
        {
            _host = host;

            _pipeline.Dispatch += OnDispatchMessage;
            _pipeline.Deliver += OnDeliverMessage;

            var toParts = _host.Split(',');

            _listener = new TcpListener(IPAddress.Parse(toParts[0]), int.Parse(toParts[1]));
            _listener.Start();

            Task.Factory.StartNew(
                    TcpAcceptConnectionsLoop, 
                    _ts.Token, 
                    TaskCreationOptions.LongRunning, 
                    TaskScheduler.Default
                );

            _logger.Info(_host + " started");
        }

        private void TcpAcceptConnectionsLoop()
        {
            while (!_ts.Token.IsCancellationRequested)
            {
                var client = _listener.AcceptTcpClient();

                Task.Factory.StartNew(
                        () => { TcpClientConnectionLoop(client); },
                        _ts.Token,
                        TaskCreationOptions.LongRunning,
                        TaskScheduler.Default
                    );
            }
        }

        private void TcpClientConnectionLoop(TcpClient client)
        {
            using (client)
            using (var stream = client.GetStream())
            using (var reader = new StreamReader(stream))
            using (var writer = new StreamWriter(stream) { AutoFlush = true })
            {
                while (!_ts.Token.IsCancellationRequested)
                {
                    string request = string.Empty;

                    try
                    {
                        request = reader.ReadLine();

                        if (string.IsNullOrEmpty(request))
                            continue;

                        _logger.Debug(_host + " r req:" + request);

                        var req = _serializer.FromString(request);

                        _pipeline.Delivery(req);

                        var rep = req.BuildReplay("ACK");

                        var replay = _serializer.ToString(rep);

                        _logger.Debug(_host + " s rep:" + replay);
                        writer.WriteLine(replay);
                    }
                    catch (JsonReaderException e)
                    {
                        _logger.Debug(_host + " r ignoring malformed req:" + request);
                    }
                }
            }
        }

        private void OnDeliverMessage(TransportMessage message)
        {
            if (Deliver != null)
                Deliver(message);
        }

        private void OnDispatchMessage(TransportMessage msg)
        {
            Task.Factory.StartNew(() => { ShipIt(msg); }, _ts.Token);
        }

        private void ShipIt(TransportMessage msg)
        {
            try
            {
                TcpClient client = null;

                lock (_lockObject)
                {
                    if (Destinations.ContainsKey(msg.To) == false)
                        client = OpenTcpConnection(msg.To);
                    else
                    {
                        client = Destinations[msg.To];

                        if (!IsTcpConnectionOpen(client))
                            client = OpenTcpConnection(msg.To);
                    }

                    if (client != null)
                        Destinations[msg.To] = client;
                    else
                        return;
                }
                
                var stream = client.GetStream();
                var reader = new StreamReader(stream);
                var writer = new StreamWriter(stream) { AutoFlush = true };

                msg.From = _host;

                var request = _serializer.ToString(msg);

                _logger.Debug(_host + " s req:" + request);
                writer.WriteLine(request);

                var replay = reader.ReadLine();
                _logger.Debug(_host + " r rep:" + replay);

                if (replay == null)
                    return;

                var rep = _serializer.FromString(replay);

                _pipeline.Delivery(rep);
            }
            catch (IOException e)
            {
                _logger.Info(_host + " busy: " + msg.To);
            }
            catch (SocketException e)
            {
                _logger.Info(_host + " unreachble: " + msg.To);
            }
        }

        private TcpClient OpenTcpConnection(string to)
        {
            var toParts = to.Split(',');

            var client = new TcpClient { ReceiveTimeout = 150 };

            if (!client.ConnectAsync(toParts[0], int.Parse(toParts[1])).Wait(250))
            {
                _logger.Info(_host + " unreachble: " + to);
                return null;
            }

            _logger.Info(_host + " established connection to : " + to);
            return client;
        }

        public bool IsTcpConnectionOpen(string dest)
        {
            if (!Destinations.ContainsKey(dest))
                return false;

            var client = Destinations[dest];

            return IsTcpConnectionOpen(client);
        }

        private bool IsTcpConnectionOpen(TcpClient client)
        {
            try
            {
                if (client != null && client.Client != null && client.Client.Connected)
                {
                    if (client.Client.Poll(0, SelectMode.SelectRead))
                    {
                        var buff = new byte[1];
                        return client.Client.Receive(buff, SocketFlags.Peek) != 0;
                    }

                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public void Send(TransportMessage msg)
        {
            _pipeline.Send(msg);
        }

        public void Dispose()
        {
            foreach (var client in Destinations.ToList())
                client.Value.Close();

            _listener.Stop();
            _ts.Cancel();
            _pipeline.Dispose();

            _logger.Info(_host + " disposed ");
        }
    }
}