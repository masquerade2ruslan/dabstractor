﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;

namespace Dabstractor.P2pLinks
{
    public class PerfectLink : IDisposable
    {
        private readonly string _host;
        private readonly StubbornLink _sl;
        public event DeliveryHandler Deliver;

        private const int DeduplicationTimeout = 5000;

        readonly List<string> _delivered = new List<string>();
        readonly object _lockObject = new object();

        readonly Pipeline<TransportMessage> _pipeline = new Pipeline<TransportMessage>();

        private static readonly Logger Logger = LogManager.GetLogger("p");

        public PerfectLink(string host)
        {
            _host = host;

            _sl = new StubbornLink(host);
            _sl.Deliver += StubbornLink_Delivery;

            _pipeline.Timeout += TimeoutExpired;
        }

        void TimeoutExpired(object message)
        {
            lock (_lockObject)
            {
                if (Logger.IsDebugEnabled)
                    Logger.Debug(_host + " deduplication window expired for " + message);

                _delivered.Remove((string) message);
            }
        }

        void StubbornLink_Delivery(TransportMessage message)
        {
            if (Logger.IsDebugEnabled)
                Logger.Debug(_host + " delivery " + message.Payload);

            lock (_lockObject)
            {
                if (_delivered.Any(x => x == message.Id))
                {
                    Logger.Debug(_host + " duplicate, dropping");
                    return;
                }    
            }

            if (Deliver != null)
            {
                lock (_lockObject)
                {
                    _delivered.Add(message.Id);
                    _pipeline.RequestTimeout(DeduplicationTimeout, message.Id);
                }

                Logger.Info(_host + " deliver " + message.Payload + " from " + message.From);
                Deliver(message);
            }
        }

        public void Send(TransportMessage message)
        {
            Logger.Info(_host + " send " + message.Payload + " to " + message.To);

            _sl.Send(message);
        }
        
        public void Dispose()
        {
            _pipeline.Dispose();
            _sl.Dispose();

            Logger.Info(_host + " disposed");
        }
    }
}
