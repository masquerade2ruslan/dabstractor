using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Dabstractor.P2pLinks
{
    public class Pipeline<T>
    {
        public readonly BlockingCollection<T> In = new BlockingCollection<T>();
        public readonly BlockingCollection<T> Out = new BlockingCollection<T>();

        readonly CancellationTokenSource _ts = new CancellationTokenSource();

        public delegate void PipelineHandler(T message);
        public delegate void TimeoutHandler(object message);

        public event PipelineHandler Dispatch;
        public event PipelineHandler Deliver;
        public event TimeoutHandler Timeout;

        private bool disposed;

        public Pipeline() : this(true) { }

        public Pipeline(bool dispatchEnabled)
        {
            if (dispatchEnabled)
            {
                Task.Factory.StartNew(DispatchLoop, _ts.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
                Task.Factory.StartNew(DeliveryLoop, _ts.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
            }
        }

        private void DeliveryLoop()
        {
            while (!_ts.Token.IsCancellationRequested)
            {
                try
                {
                    T msg = default(T);

                    if (Out.TryTake(out msg, -1))
                    {
                        if (!disposed && Deliver != null)
                        {
                            Deliver(msg);
                        }
                    }
                }

                catch (OperationCanceledException)
                {
                    break;
                }
            }
        }

        private void DispatchLoop()
        {
            while (!_ts.Token.IsCancellationRequested)
            {
                try
                {
                    T msg = default(T);

                    if (In.TryTake(out msg, -1))
                    {
                        if (!disposed && Dispatch != null)
                            Dispatch(msg);
                    }
                }
                catch (OperationCanceledException)
                {
                    break;
                }
            }
        }

        public void Send(T msg)
        {
            In.Add(msg);
        }

        public void Delivery(T msg)
        {
            Out.Add(msg);
        }

        public void RequestTimeout(int mlsc, object state)
        {
            Task.Delay(mlsc).ContinueWith(c =>
            {
                if (!disposed && Timeout != null)
                    Timeout(state);
            });
        }

        public void Dispose()
        {
            disposed = true;
            _ts.Cancel();

            /*while (In.Count > 0)
            {
                T item;
                In.TryTake(out item);
            }

            while (Out.Count > 0)
            {
                T item;
                In.TryTake(out item);
            }*/
        }
    }
}