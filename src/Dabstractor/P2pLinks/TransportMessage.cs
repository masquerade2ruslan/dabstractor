﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Dabstractor.P2pLinks
{
    public class TransportMessage
    {
        public string Id { get; set; } 
        public string CorrelationId { get; set; }

        public string Payload { get; set; }

        public string From { get; set; } 
        public string To { get; set; }
        public int Seq { get; set; }

        public Dictionary<string, string> Headers = new Dictionary<string, string>(); 

        public TransportMessage()
        {
            var id = Guids.GenerateComb();

            Id = id;
            CorrelationId = id;
        }

        public TransportMessage(string payload) : this("", payload) { }

        public TransportMessage(string to, string payload) :this()
        {
            To = to;
            Payload = payload;
        }

        public TransportMessage BuildReplay(string payload)
        {
            var from = From;
            var to = To;
            var id = Id;

            return new TransportMessage()
            {
                Id = Guids.GenerateComb(),
                CorrelationId = id,
                From = to,
                To = from,
                Payload = payload
            };
        }

        public TransportMessage Clone(Action<TransportMessage> tweak)
        {
            var serializer = new TransportMessageSerializer();

            var cloned = serializer.Clone(this);
            tweak(cloned);

            return cloned;
        }
    }

    public class TransportMessageSerializer
    {
        public TransportMessage FromString(string payload)
        {
            return JsonConvert.DeserializeObject<TransportMessage>(payload);
        }
        
        public string ToString(TransportMessage msg)
        {
            return JsonConvert.SerializeObject(msg);
        }

        public TransportMessage Clone(TransportMessage target)
        {
            return FromString(ToString(target));
        }
    }
}