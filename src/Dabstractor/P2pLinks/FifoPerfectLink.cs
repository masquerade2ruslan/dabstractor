using System;
using System.Collections.Generic;
using System.Linq;
using NLog;

namespace Dabstractor.P2pLinks
{
    public class FifoPerfectLink : IDisposable
    {
        private readonly string _host;
        private readonly PerfectLink _pl;

        private int _seq = 0;
        private int _next = 1;

        private readonly List<TransportMessage> _pending = new List<TransportMessage>();

        public event DeliveryHandler Deliver;

        private static readonly Logger Logger = LogManager.GetLogger("fp");

        public FifoPerfectLink(string host)
        {
            _host = host;
            _pl = new PerfectLink(host);
            _pl.Deliver += PerfectLink_Delivery;
        }

        private void PerfectLink_Delivery(TransportMessage message)
        {
            if (Deliver != null)
            {
                _pending.Add(message);

                Logger.Info(_host + " pending " + message.Seq + " " + message.Payload + " from " + message.From);

                foreach (var tm in _pending.ToList())
                {
                    var nextTm = _pending.FirstOrDefault(x => x.Seq == _next);

                    if (nextTm != null)
                    {
                        _pending.Remove(nextTm);

                        Logger.Info(_host + " deliver " + _next + " " + nextTm.Payload + " from " + nextTm.From);
                        Deliver(nextTm);

                        _next++;
                    }
                }
            }
        }

        public void Send(TransportMessage message)
        {
            _seq++;
            message.Seq = _seq;

            Logger.Info(_host + " send " + _seq + " " + message.Payload + " to " + message.To);
            _pl.Send(message);
        }

        public void Dispose()
        {
            _pl.Dispose();

            Logger.Info(_host + " disposed");
        }
    }
}