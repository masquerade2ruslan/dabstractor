﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;

namespace Dabstractor.P2pLinks
{
    public delegate void DeliveryHandler(TransportMessage message);

    public class StubbornLink : IDisposable
    {
        private readonly Logger Logger = LogManager.GetLogger("s");

        readonly TransportMessageSerializer _serializer = new TransportMessageSerializer();
        readonly Pipeline<TransportMessage> _pipeline = new Pipeline<TransportMessage>();

        private readonly FairLossLink _fll = null;

        readonly List<TransportMessage> _unacknowledged = new List<TransportMessage>();
        readonly object _lockObject = new object();

        private readonly string _host;

        public event DeliveryHandler Deliver;


        public StubbornLink(string host)
        {
            this._host = host;

            _pipeline.Dispatch += Pipeline_Dispatch;
            _pipeline.Deliver += Pipeline_Deliver;
            _pipeline.Timeout += Pipeline_Timeout;

            _fll = new FairLossLink(_host);
            _fll.Deliver += message => _pipeline.Delivery(message);
        }

        public void Send(TransportMessage message)
        {
            if (Logger.IsDebugEnabled)
                Logger.Debug(_host + " send " + _serializer.ToString(message));

            _pipeline.Send(message);
        }

        private void Pipeline_Deliver(TransportMessage message)
        {
            if (Logger.IsDebugEnabled)
                Logger.Debug(_host + " delivery " + _serializer.ToString(message));

            try
            {
                if (message.Payload == "ACK")
                {
                    lock (_lockObject)
                    {
                        var req = _unacknowledged.FirstOrDefault(x => x.Id == message.CorrelationId);

                        if (req == null)
                            Logger.Debug(_host + " acknowledged and delivered #" + message.CorrelationId);
                        else
                        {
                            _unacknowledged.Remove(req);
                            Logger.Debug(_host + " delivered #" + req.Id);
                        }
                    }
                }
                else if (Deliver != null)
                {
                    Logger.Debug(_host + " delivery " + message.Payload);

                    Deliver(message);
                }
            }
            catch (Exception e)
            {
                Logger.Info(_host + " " + _serializer.ToString(message) + " " + e);
            }
        }

        private void Pipeline_Dispatch(TransportMessage message)
        {
            if (Logger.IsDebugEnabled)
                Logger.Debug(_host + " dispatch " + _serializer.ToString(message));

            lock (_lockObject)
                _unacknowledged.Add(message);
            
            _fll.Send(message);
            _pipeline.RequestTimeout(150, message);
        }


        private void Pipeline_Timeout(object obj)
        {
            if (obj is TransportMessage)
            {
                var message = (TransportMessage) obj;

                if (_unacknowledged.Any(x => x.Id == message.Id))
                {
                    if (Logger.IsDebugEnabled)
                        Logger.Debug(_host + " resend " + _serializer.ToString(message));

                    _fll.Send(message);
                    _pipeline.RequestTimeout(250, message);
                }
            }
        }

        public bool IsSendComplete(string destination, string messageId)
        {
            return _unacknowledged.All(x => x.Id != messageId);
        }

        public void Dispose()
        {
            _pipeline.Dispose();
            _fll.Dispose();

            Logger.Info(_host + " disposed");
        }
    }
}