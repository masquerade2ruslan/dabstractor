﻿using System.Collections.Generic;
using System.Threading;
using Dabstractor.FailureDetectors;
using NLog;
using NUnit.Framework;

namespace Dabstractor.Specs.FailureDetectors
{
    [TestFixture]
    public class PerfectFailureDetectorSpec
    {
        private static readonly Logger Logger = LogManager.GetLogger("tst");

        [Test]
        public void ATest()
        {
            var d1Detected = false;
            var d2Detected = false;

            var pfd1 = new PerfectFailureDetector("10.10.10.1,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });
            var pfd2 = new PerfectFailureDetector("10.10.10.2,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });
            var pfd3 = new PerfectFailureDetector("10.10.10.3,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });

            pfd1.Crash += peer => d1Detected = peer.Equals("10.10.10.3,2555");
            pfd2.Crash += peer => d2Detected = peer.Equals("10.10.10.3,2555");

            Thread.Sleep(1000);

            pfd3.Dispose();
            Logger.Info("dispose called");

            while (!(d2Detected && d1Detected))
                Thread.Sleep(500);

            pfd1.Dispose();
            pfd2.Dispose();

        }
    }
}