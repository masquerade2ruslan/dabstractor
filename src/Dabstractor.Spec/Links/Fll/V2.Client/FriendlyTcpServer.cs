using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Dabstractor.Specs.Links.Fll.V2.Client
{
    public class FriendlyTcpServer : IDisposable
    {
        TcpListener _listener = null;
        TcpClient client;

        AutoResetEvent disposed = new AutoResetEvent(false);
        readonly CancellationTokenSource _ts = new CancellationTokenSource();

        public FriendlyTcpServer(string endPoint, Action<StreamReader, StreamWriter> loop)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    var toParts = endPoint.Split(',');
                    _listener = new TcpListener(IPAddress.Parse(toParts[0]), int.Parse(toParts[1]));
                    _listener.Start();
                    Console.WriteLine("DBG started");

                    client = _listener.AcceptTcpClient();

                    {
                        Console.WriteLine("DBG got connection");

                        using (var stream = client.GetStream())
                        {
                            Console.WriteLine("DBG got client stream");

                            using (var reader = new StreamReader(stream))
                            using (var writer = new StreamWriter(stream) { AutoFlush = true })
                            {
                                while (!_ts.Token.IsCancellationRequested)
                                {
                                    loop(reader, writer);
                                }
                            }
                        }
                    }
                }
                catch (SocketException e)
                {
                }
                catch (IOException e)
                {
                }
                catch (Exception e)
                {
                    Console.WriteLine("!!!!!!!!!" + e);
                }
                finally
                {
                    _listener.Stop();
                    disposed.Set();
                }
            }, _ts.Token);
        }

        public void Dispose()
        {
            Console.WriteLine("DBG disposing");

            _listener.Stop();
            client.Close();
            _ts.Cancel();

            disposed.WaitOne();
        }
    }
}