﻿using System.Threading;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Fll.V2.Client
{
    [TestFixture]
    public class SendWhenServerUnreacheableSpec
    {
        FairLossLink _fll;

        [SetUp]
        public void Given()
        {
            _fll = new FairLossLink("10.10.10.2,2555");
            _fll.Send(new TransportMessage("10.10.10.3,2555", "alive"));

            Thread.Sleep(500);
        }

        [Test]
        public void DoesNotCacheClientInDestinations()
        {
            Assert.IsFalse(_fll.Destinations.ContainsKey("10.10.10.3,2555"));
        }

        [TearDown]
        public void Finally()
        {
            _fll.Dispose();
        }
    }
}