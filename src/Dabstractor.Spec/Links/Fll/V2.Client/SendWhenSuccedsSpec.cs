using System;
using System.Threading;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Fll.V2.Client
{
    [TestFixture]
    public class SendWhenSuccedsSpec
    {
        FairLossLink _fll;
        TransportMessage _replyToSend = null;

        
        private FriendlyTcpServer _friendlyTcpServer = null;

        [SetUp]
        public void Given()
        {
            _friendlyTcpServer = new FriendlyTcpServer("10.10.10.3,2555", (reader, writer) =>
            {
                var readLine = reader.ReadLine();
                Console.WriteLine("DBG req " + readLine);

                if (readLine != null)
                {
                    Console.WriteLine("DBG responding");
                    writer.WriteLine(@"{'Id':1,'CorrelationId':1,'From':'10.10.10.3,2555','To':'10.10.10.2,2555','Payload':'ACK'}");
                }
            });

            var akked = new AutoResetEvent(false);

            _fll = new FairLossLink("10.10.10.2,2555");
            _fll.Deliver += rep =>
            {
                _replyToSend = rep;
                akked.Set();
            };

            _fll.Send(new TransportMessage("10.10.10.3,2555", "alive"));

            akked.WaitOne();
        }

        [Test]
        public void DoesCacheClientInDestinations()
        {
            Assert.IsTrue(_fll.Destinations.ContainsKey("10.10.10.3,2555"));
        }
        
        [Test]
        public void DispatchesReplay()
        {
            Assert.NotNull(_replyToSend);
        }

        [TearDown]
        public void Finally()
        {
            _fll.Dispose();
            _friendlyTcpServer.Dispose();
        }
    }
}