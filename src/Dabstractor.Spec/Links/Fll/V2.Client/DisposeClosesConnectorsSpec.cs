using System;
using System.Threading;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Fll.V2.Client
{
    [TestFixture]
    public class DisposeClosesConnectorsSpec
    {
        FairLossLink _fll;
        FriendlyTcpServer _server;

        [SetUp]
        public void Given()
        {
            _server = new FriendlyTcpServer("10.10.10.3,2555", (reader, writer) =>
            {
                var readLine = reader.ReadLine();
                Console.WriteLine("DBG req " + readLine);

                if (readLine != null)
                {
                    Console.WriteLine("DBG responding");
                    writer.WriteLine(@"{'Id':1,'CorrelationId':1,'From':'10.10.10.3,2555','To':'10.10.10.2,2555','Payload':'ACK'}");
                }
            });

            var linked = new AutoResetEvent(false);

            _fll = new FairLossLink("10.10.10.2,2555");
            _fll.Deliver += rep =>
            {
                linked.Set();
            };

            _fll.Send(new TransportMessage("10.10.10.3,2555", "alive"));

            linked.WaitOne();
        }

        [Test]
        public void Closed()
        {
            _fll.Dispose();

            Assert.IsFalse(_fll.IsTcpConnectionOpen("10.10.10.3,2555"));
        }

        [TearDown]
        public void Finally()
        {
            _server.Dispose();
        }
    }
}