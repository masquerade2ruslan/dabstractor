using System;
using System.Threading;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Fll.V2.Client
{
    [TestFixture]
    public class SendsTo2DestinationsWillOpen2ChannalsSpec
    {
        FairLossLink _fll;
        FriendlyTcpServer _s1 = null;
        FriendlyTcpServer _s2 = null;

        [SetUp]
        public void Given()
        {
            _s1 = new FriendlyTcpServer("10.10.10.1,2555", (reader, writer) =>
            {
                var readLine = reader.ReadLine();
                Console.WriteLine("DBG req " + readLine);

                if (readLine != null)
                {
                    Console.WriteLine("DBG responding");
                    writer.WriteLine(@"{'Id':1,'CorrelationId':1,'From':'10.10.10.3,2555','To':'10.10.10.2,2555','Payload':'ACK'}");
                }
            });

            _s2 = new FriendlyTcpServer("10.10.10.2,2555", (reader, writer) =>
            {
                var readLine = reader.ReadLine();
                Console.WriteLine("DBG req " + readLine);

                if (readLine != null)
                {
                    Console.WriteLine("DBG responding");
                    writer.WriteLine(@"{'Id':1,'CorrelationId':1,'From':'10.10.10.3,2555','To':'10.10.10.2,2555','Payload':'ACK'}");
                }
            });

            var akkedFirst = new AutoResetEvent(false);
            var akkedSecond = new AutoResetEvent(false);

            var attempt = 0;

            _fll = new FairLossLink("10.10.10.3,2555");
            _fll.Deliver += rep =>
            {
                attempt++;

                if (attempt == 1)
                    akkedFirst.Set();

                if (attempt == 2)
                    akkedSecond.Set();
            };

            _fll.Send(new TransportMessage("10.10.10.1,2555", "alive1"));
            _fll.Send(new TransportMessage("10.10.10.2,2555", "alive2"));

            akkedFirst.WaitOne();
            akkedSecond.WaitOne();
        }

        [Test]
        public void ConnectionReuse()
        {
            Assert.That(_fll.Destinations.Keys.Count, Is.EqualTo(2));
        }

        [TearDown]
        public void Finally()
        {
            _fll.Dispose();

            _s1.Dispose();
            _s2.Dispose();
        }
    }
}