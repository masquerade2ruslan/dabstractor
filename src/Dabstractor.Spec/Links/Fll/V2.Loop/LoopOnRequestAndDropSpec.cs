using System.IO;
using System.Net.Sockets;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Fll.V2.Loop
{
    [TestFixture]
    public class LoopOnRequestAndDropSpec
    {
        FairLossLink _fll;
        TransportMessage _req;

        [SetUp]
        public void Given()
        {
            _fll = new FairLossLink("10.10.10.2,2555");
            _fll.Deliver += rep => { _req = rep; };

            var serializer = new TransportMessageSerializer();

            using (var client = new TcpClient("10.10.10.2", 2555) { ReceiveTimeout = 150 })
            using (var stream = client.GetStream())
            using (var writer = new StreamWriter(stream) { AutoFlush = true })
            {
                var req = serializer.ToString(new TransportMessage()
                {
                    From = "10.10.10.1,2555",
                    To = "10.10.10.2,2555",
                    Payload = "hb",
                    Id = "90909"
                });

                writer.WriteLine(req);
            }
        }

        [Test]
        public void NonDelivery()
        {
            Assert.IsNull(_req);
        }

        [TearDown]
        public void Finally()
        {
            _fll.Dispose();
        }
    }
}