using System;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Fll.V2.Loop
{
    [TestFixture]
    public class LoopStartsAtCreationSpec
    {
        FairLossLink _fll;

        [SetUp]
        public void Given()
        {
            _fll = new FairLossLink("10.10.10.2,2555");
        }

        [Test]
        public void Initialized()
        {
            Assert.IsNotNull(_fll);
        }

        [TearDown]
        public void Finally()
        {
            _fll.Dispose();
        }
    }
}