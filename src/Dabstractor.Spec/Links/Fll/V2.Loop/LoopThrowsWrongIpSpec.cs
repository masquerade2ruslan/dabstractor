using System.Net.Sockets;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Fll.V2.Loop
{
    [TestFixture]
    public class LoopThrowsWrongIpSpec
    {
        [Test]
        public void IpNotOnNetworkCard()
        {
            Assert.Throws<SocketException>(() => new FairLossLink("10.10.10.7,2555"));
        }
    }
}