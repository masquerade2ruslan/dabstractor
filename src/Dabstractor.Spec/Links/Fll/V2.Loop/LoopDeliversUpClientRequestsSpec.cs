using System.IO;
using System.Net.Sockets;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Fll.V2.Loop
{
    [TestFixture]
    public class LoopDeliversUpClientRequestsSpec
    {
        FairLossLink _fll;
        TransportMessage _req2;

        [SetUp]
        public void Given()
        {
            _fll = new FairLossLink("10.10.10.2,2555");
            _fll.Deliver += req => { _req2 = req; };

            using (var client = new TcpClient("10.10.10.2", 2555) { ReceiveTimeout = 150 })
            using (var stream = client.GetStream())
            using (var writer = new StreamWriter(stream) { AutoFlush = true })
            using (var reader = new StreamReader(stream))
            {
                var req = new TransportMessageSerializer().ToString(new TransportMessage()
                {
                    From = "10.10.10.1,2555",
                    To = "10.10.10.2,2555",
                    Payload = "hb",
                    Id = "9090"
                });

                writer.WriteLine(req);
                reader.ReadLine();
            }
        }

        [Test]
        public void Delivery()
        {
            Assert.IsNotNull(_req2);
        }

        [Test]
        public void RespectedFrom()
        {
            Assert.That(_req2.From, Is.EqualTo("10.10.10.1,2555"));
        }

        [Test]
        public void RespectedTo()
        {
            Assert.That(_req2.To, Is.EqualTo("10.10.10.2,2555"));
        }

        [Test]
        public void RespectedPayload()
        {
            Assert.That(_req2.Payload, Is.EqualTo("hb"));
        }

        [Test]
        public void RespectedId()
        {
            Assert.That(_req2.Id, Is.EqualTo("9090"));
        }

        [TearDown]
        public void Finally()
        {
            _fll.Dispose();
        }
    }
}