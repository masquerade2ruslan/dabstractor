using System.IO;
using System.Net.Sockets;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Fll.V2.Loop
{
    [TestFixture]
    public class LoopAcksOnRequestsSpec
    {
        FairLossLink _fll;
        TransportMessage _rep;

        [SetUp]
        public void Given()
        {
            _fll = new FairLossLink("10.10.10.2,2555");

            using (var client = new TcpClient("10.10.10.2", 2555) { ReceiveTimeout = 150 })
            using (var stream = client.GetStream())
            using (var writer = new StreamWriter(stream) { AutoFlush = true })
            using (var reader = new StreamReader(stream))
            {
                var serializer = new TransportMessageSerializer();

                var req = serializer.ToString(new TransportMessage()
                {
                    From = "10.10.10.1,2555",
                    To = "10.10.10.2,2555",
                    Payload = "hb",
                    Id = "90909"
                });

                writer.WriteLine(req);
                var rep = reader.ReadLine();

                _rep = serializer.FromString(rep);
            }
        }

        [Test]
        public void Acked()
        {
            Assert.That(_rep.Payload, Is.EqualTo("ACK"));
        }
        
        [Test]
        public void RespectsCorrelationId()
        {
            Assert.That(_rep.CorrelationId, Is.EqualTo("90909"));

        }

        [TearDown]
        public void Finally()
        {
            _fll.Dispose();
        }
    }
}