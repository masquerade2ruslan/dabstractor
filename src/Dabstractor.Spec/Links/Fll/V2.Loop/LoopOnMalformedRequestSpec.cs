using System.IO;
using System.Net.Sockets;
using System.Threading;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Fll.V2.Loop
{
    [TestFixture]
    public class LoopOnMalformedRequestSpec
    {
        FairLossLink _fll;
        TransportMessage _req;

        [SetUp]
        public void Given()
        {
            _fll = new FairLossLink("10.10.10.2,2555");
            _fll.Deliver += rep => { _req = rep; };

            using (var client = new TcpClient("10.10.10.2", 2555) { ReceiveTimeout = 150 })
            using (var stream = client.GetStream())
            using (var writer = new StreamWriter(stream) { AutoFlush = true })
            using (var reader = new StreamReader(stream))
            {
                writer.WriteLine("fwwqer4353252234dsdf");

                Thread.Sleep(1000);
            }
        }

        [Test]
        public void NonDelivery()
        {
            Assert.IsNull(_req);
        }

        [TearDown]
        public void Finally()
        {
            _fll.Dispose();
        }
    }
}