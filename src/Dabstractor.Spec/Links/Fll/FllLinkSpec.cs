using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Fll
{
    [TestFixture]
    public class FllLinkSpec
    {
        [Test]
        public void OneMessageTest()
        {
            TransportMessage got = null;

            using (var p = new FairLossLink("10.10.10.1,2555"))
            using (var q = new FairLossLink("10.10.10.2,2555"))
            {
                q.Deliver += message => got = message;

                p.Send(new TransportMessage("10.10.10.2,2555", "alive"));

                while (got == null)
                    Thread.Sleep(100);
                
            }

            Assert.NotNull(got);
        }

        [Test]
        public void FiveMessageTest()
        {
            using (var p = new FairLossLink("10.10.10.1,2555"))
            using (var q = new FairLossLink("10.10.10.2,2555"))
            using (var r = new FairLossLink("10.10.10.3,2555"))
            {
                q.Deliver += message => Console.WriteLine(">>" + message.Payload);
                r.Deliver += message => Console.WriteLine(">>" + message.Payload);

                p.Send(new TransportMessage("10.10.10.2,2555", "111"));
                p.Send(new TransportMessage("10.10.10.3,2555", "222"));
                p.Send(new TransportMessage("10.10.10.2,2555", "333"));

                Thread.Sleep(1000);
                    

            }
        }

        [Test]
        public void CleanTest()
        {
            var p = new FairLossLink("10.10.10.1,2555");
            var q = new FairLossLink("10.10.10.2,2555");
            
            p.Dispose();
            q.Dispose();
        }
    }
}