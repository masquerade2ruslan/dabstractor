﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Sl
{
    [TestFixture]
    public class StubbornLinkUsageSample
    {
        [Test]
        public void SimpleTest()
        {
            using (var sl1 = new StubbornLink("10.10.10.2,2555")) // A node
            using (var sl2 = new StubbornLink("10.10.10.3,2555")) // B node
            {
                TransportMessage response = null;

                sl2.Deliver += msg => response = msg;

                Thread.Sleep(500);

                sl1.Send(new TransportMessage("10.10.10.3,2555", "alive"));  // send to B "alive"

                Thread.Sleep(500);

                Assert.AreEqual("alive", response.Payload);
            }
        }

        [Test]
        public void WireProtocolWithAckLossesSpec()
        {
            TransportMessage message = new TransportMessage("10.10.10.3,2555", "ping");

            using (var sl = new StubbornLink("10.10.10.2,2555"))
            {
                Task.Factory.StartNew(() =>
                {
                    {
                        var _listener = new TcpListener(IPAddress.Parse("10.10.10.3"), 2555);
                        _listener.Start();

                        for (int i = 0; i < 4; i++)
                        {
                            using (var client = _listener.AcceptTcpClient())
                            {
                                using (var stream = client.GetStream())
                                {
                                    using (var reader = new StreamReader(stream))
                                    using (var writer = new StreamWriter(stream) { AutoFlush = true })
                                    {
                                        try
                                        {
                                            var readLine = reader.ReadLine();

                                            if (i % 2 != 0)
                                            {
                                                Console.WriteLine("S acking");
                                                writer.WriteLine(@"{'Id':1,'CorrelationId':'" + message.Id + "','From':'10.10.10.3,2555','To':'10.10.10.2,2555','Payload':'ACK'}");
                                            }
                                            else
                                                Console.WriteLine("S loss");
                                        }
                                        catch (IOException e) { }
                                    }
                                }

                            }
                        }

                        _listener.Stop();

                    }
                });


                sl.Send(message);

                Thread.Sleep(3000);

                Assert.IsTrue(sl.IsSendComplete(message.To, message.Id));

            }
        }
    }
}