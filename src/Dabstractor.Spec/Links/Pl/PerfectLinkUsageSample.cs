﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Pl
{
    [TestFixture]
    public class PerfectLinkUsageSample
    {
        [Test]
        public void SimpleTest()
        {
            using (var sl1 = new PerfectLink("10.10.10.2,2555")) // A node
            using (var sl2 = new PerfectLink("10.10.10.3,2555")) // B node
            {
                TransportMessage response = null;

                sl2.Deliver += msg => response = msg;

                sl1.Send(new TransportMessage("10.10.10.3,2555","alive")); // send to A "alive"

                Thread.Sleep(1500);

                Assert.AreEqual("alive", response.Payload);
            }
        }

    }
}