﻿using System.Collections.Generic;
using System.Threading;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links.Fpl
{
    [TestFixture]
    public class FifoPerfectLinkSpec
    {
        [Test]
        public void SendMessagesTest()
        {
            var received = new List<TransportMessage>();

            using (var q = new FifoPerfectLink("10.10.10.2,2555"))
            using (var p = new FifoPerfectLink("10.10.10.1,2555"))
            {
                q.Deliver += message => received.Add(message);

                p.Send(new TransportMessage("10.10.10.2,2555", "alive"));
                p.Send(new TransportMessage("10.10.10.2,2555", "alive"));

               Thread.Sleep(450);
            }

            Assert.AreEqual(1, received[0].Seq);
            Assert.AreEqual(2, received[1].Seq);
        }

        [Test]
        public void ReorderingTest()
        {
            var received = new List<TransportMessage>();

            using (var q = new FifoPerfectLink("10.10.10.2,2555"))
            using (var p = new PerfectLink("10.10.10.1,2555"))
            {
                q.Deliver += message => received.Add(message);

                p.Send(new TransportMessage("10.10.10.2,2555", "alive"){ Seq = 2 });
                p.Send(new TransportMessage("10.10.10.2,2555", "alive") { Seq = 1 });

                Thread.Sleep(450);
            }

            Assert.AreEqual(1, received[0].Seq);
            Assert.AreEqual(2, received[1].Seq);
        }

        [Test]
        public void BuffersTest()
        {
            var received = new List<TransportMessage>();

            using (var q = new FifoPerfectLink("10.10.10.2,2555"))
            using (var p = new PerfectLink("10.10.10.1,2555"))
            {
                q.Deliver += message => received.Add(message);

                p.Send(new TransportMessage("10.10.10.2,2555", "alive") { Seq = 2 });

                Thread.Sleep(450);
            }

            Assert.AreEqual(0, received.Count);
        }

        [Test]
        public void DeliverAndBuffersTest()
        {
            var received = new List<TransportMessage>();

            using (var q = new FifoPerfectLink("10.10.10.2,2555"))
            using (var p = new PerfectLink("10.10.10.1,2555"))
            {
                q.Deliver += message => received.Add(message);

                p.Send(new TransportMessage("10.10.10.2,2555", "1") { Seq = 1 });
                p.Send(new TransportMessage("10.10.10.2,2555", "2") { Seq = 2 });
                p.Send(new TransportMessage("10.10.10.2,2555", "5") { Seq = 5 });

                Thread.Sleep(450);
            }

            Assert.AreEqual(2, received.Count);
        }
    }
}