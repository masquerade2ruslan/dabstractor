using System.Threading;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Links
{
    [TestFixture]
    public class PipelineSpec
    {
        [Test]
        public void SendWillEnqueueForProcessingSpec()
        {
            var pipe = new Pipeline<string>(false);
            pipe.Send("me");

            Assert.AreEqual(1, pipe.In.Count);
        }

        [Test]
        public void SendWillDisatchToMainLoopSpec()
        {
            var dispatched = string.Empty;

            var pipe = new Pipeline<string>();
            pipe.Dispatch += msg => { dispatched = msg; };
            pipe.Send("me");

            Thread.Sleep(100);

            Assert.AreEqual("me", dispatched);
        }

        [Test]
        public void OutWillDeliverLoopSpec()
        {
            var delivered = string.Empty;

            var pipe = new Pipeline<string>();
            pipe.Deliver += msg => { delivered = msg; };
            
            pipe.Out.Add("me");

            Thread.Sleep(100);

            Assert.AreEqual("me", delivered);
        }

        [Test]
        public void RequestTimeoutAfterSpec()
        {
            object delivered = null;

            var pipe = new Pipeline<string>(false);
            pipe.Timeout += msg => { delivered = msg; };
            pipe.RequestTimeout(100, "me");

            Thread.Sleep(150);

            Assert.NotNull(delivered);

        }

        [Test]
        public void RequestTimeoutBeforeSpec()
        {
            object delivered = null;

            var pipe = new Pipeline<string>(false);
            pipe.Timeout+= msg => { delivered = msg; };
            pipe.RequestTimeout(100, "me");

            Thread.Sleep(80);

            Assert.Null(delivered);
        }


        [Test]
        public void DisposedDoesNotFireTimeoutsInFlightSpec()
        {
            object delivered = null;

            var pipe = new Pipeline<string>(false);
            pipe.Timeout += msg => { delivered = msg; };
            pipe.RequestTimeout(100, "me");
            pipe.Dispose();

            Thread.Sleep(180);

            Assert.Null(delivered);
        }

    }
}