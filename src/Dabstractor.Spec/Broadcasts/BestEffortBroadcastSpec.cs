﻿using System.Collections.Generic;
using System.Threading;
using Dabstractor.Broadcasts;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Broadcasts
{
    [TestFixture]
    public class BestEffortBroadcastSpec
    {
        [Test]
        public void HowTo()
        {
            var peers = new List<string>() {"10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555"};

            TransportMessage pm = null;
            TransportMessage qm = null;
            TransportMessage rm = null;

            using (var p = new BestEffortBroadcast("10.10.10.1,2555", peers))
            using (var q = new BestEffortBroadcast("10.10.10.2,2555", peers))
            using (var r = new BestEffortBroadcast("10.10.10.3,2555", peers))
            {
                p.Deliver += message => pm = message;
                q.Deliver += message => qm = message;
                r.Deliver += message => rm = message;

                p.Broadcast(new TransportMessage() { Payload = "yay" });

                Thread.Sleep(300);
            }

            Assert.NotNull(pm);
            Assert.NotNull(qm);
            Assert.NotNull(rm);
        }

        [Test]
        public void WhnOneNodeIsUnreacheable()
        {
            var peers = new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" };

            TransportMessage pm = null;
            TransportMessage qm = null;

            using (var p = new BestEffortBroadcast("10.10.10.1,2555", peers))
            using (var q = new BestEffortBroadcast("10.10.10.2,2555", peers))
            {
                p.Deliver += message => pm = message;
                q.Deliver += message => qm = message;

                p.Broadcast(new TransportMessage() { Payload = "yay" });

                Thread.Sleep(300);
            }

            Assert.NotNull(pm);
            Assert.NotNull(qm);
        }
    }
}