﻿using System.Collections.Generic;
using System.Threading;
using Dabstractor.Broadcasts;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs.Broadcasts
{
    [TestFixture]
    public class UniformReliableBroadcastSpec
    {
        [Test]
        public void WhenAllAlive()
        {
            var peers = new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" };

            TransportMessage pm = null;
            TransportMessage qm = null;
            TransportMessage rm = null;

            using (var p = new UniformReliableBroadcast("10.10.10.1,2555", peers))
            using (var q = new UniformReliableBroadcast("10.10.10.2,2555", peers))
            using (var r = new UniformReliableBroadcast("10.10.10.3,2555", peers))
            {
                p.Deliver += message => pm = message;
                q.Deliver += message => qm = message;
                r.Deliver += message => rm = message;

                p.Broadcast(new TransportMessage() { Payload = "yay" });

                Thread.Sleep(400);
            }

            Assert.NotNull(pm);
            Assert.NotNull(qm);
            Assert.NotNull(rm);
        }

        [Test]
        public void Minority()
        {
            var peers = new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" };

            TransportMessage pm = null;
            TransportMessage qm = null;

            using (var p = new UniformReliableBroadcast("10.10.10.1,2555", peers))
            using (var q = new UniformReliableBroadcast("10.10.10.2,2555", peers))
            {
                p.Deliver += message => pm = message;
                q.Deliver += message => qm = message;

                p.Broadcast(new TransportMessage() { Payload = "yay" });

                Thread.Sleep(400);
            }

            Assert.NotNull(pm);
            Assert.NotNull(qm);
        }

        [Test]
        public void InMinorityWillNotDeliver()
        {
            var peers = new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" };

            TransportMessage pm = null;

            using (var p = new UniformReliableBroadcast("10.10.10.1,2555", peers))
            {
                p.Deliver += message => pm = message;

                p.Broadcast(new TransportMessage() { Payload = "yay" });

                Thread.Sleep(400);
            }

            Assert.IsNull(pm);
        }
    }
}