using System.Collections.Generic;
using System.Threading;
using Dabstractor.LeaderElection;
using NUnit.Framework;

namespace Dabstractor.Specs.LeaderElection
{
    [TestFixture]
    public class EventualLeaderDetectorSpec
    {
        EventualLeaderDetector _le1;

        [SetUp]
        public void Given()
        {
            _le1 = new EventualLeaderDetector("10.10.10.1,2555", new List<string>() { "10.10.10.1,2555" });
        }

        [Test]
        public void OneNodeBecomesLeaderTest()
        {
            Thread.Sleep(1000);

            Assert.That(_le1.CurrentLeader, Is.EqualTo("10.10.10.1,2555"));
        }

        [TearDown]
        public void Finally()
        {
            _le1.Dispose();
        }
    }

    [TestFixture]
    public class TandemEventualLeaderDetectorSpec
    {
        EventualLeaderDetector _le1;
        EventualLeaderDetector _le2;

        [SetUp]
        public void Given()
        {
            _le1 = new EventualLeaderDetector("10.10.10.1,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555" });
            _le2 = new EventualLeaderDetector("10.10.10.2,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555" });
        }

        [Test]
        public void EpochWillInfleunceLeaderElectionTest()
        {
            Thread.Sleep(1000);

            Assert.That(_le1.CurrentLeader, Is.EqualTo("10.10.10.2,2555"));
            Assert.That(_le2.CurrentLeader, Is.EqualTo("10.10.10.2,2555"));
        }

        [TearDown]
        public void Finally()
        {
            _le2.Dispose();
            _le1.Dispose();
        }
    }

    [TestFixture]
    public class DetectFailedLeaderTandemEventualLeaderDetectorSpec
    {
        EventualLeaderDetector _le1;
        EventualLeaderDetector _le2;

        [SetUp]
        public void Given()
        {
            _le1 = new EventualLeaderDetector("10.10.10.1,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555" });
            _le2 = new EventualLeaderDetector("10.10.10.2,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555" });
        }

        [Test]
        public void DetectsTheFauleOfLeaderTest()
        {
            Thread.Sleep(1000);
            _le2.Dispose();

            Thread.Sleep(1000);

            Assert.That(_le1.CurrentLeader, Is.EqualTo("10.10.10.1,2555"));
        }

        [TearDown]
        public void Finally()
        {
            _le1.Dispose();
        }
    }
}