using System.Collections.Generic;
using System.Threading;
using Dabstractor.LeaderElection;
using NUnit.Framework;

namespace Dabstractor.Specs.LeaderElection
{
    [TestFixture]
    public class LeaderElectionOneProcessWillLeadSpec
    {
        MonarchicalLeaderElection _le1;

        [SetUp]
        public void Given()
        {
            _le1 = new MonarchicalLeaderElection("10.10.10.1,2555", new List<string>() { "10.10.10.1,2555" });
        }

        [Test]
        public void Test()
        {
            Thread.Sleep(1000);

            Assert.That(_le1.CurrentLeader, Is.EqualTo("10.10.10.1,2555"));
        }

        [TearDown]
        public void Finally()
        {
            _le1.Dispose();
        }
    }

    [TestFixture]
    public class LeaderElectionWillLeadTheMaxIpSpec
    {
        MonarchicalLeaderElection _le1;
        MonarchicalLeaderElection _le2;

        [SetUp]
        public void Given()
        {
            _le1 = new MonarchicalLeaderElection("10.10.10.1,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555" });
            _le2 = new MonarchicalLeaderElection("10.10.10.2,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555" });
        }

        [Test]
        public void Test()
        {
            Thread.Sleep(1000);

            Assert.That(_le1.CurrentLeader, Is.EqualTo("10.10.10.2,2555"));
            Assert.That(_le2.CurrentLeader, Is.EqualTo("10.10.10.2,2555"));
        }

        [TearDown]
        public void Finally()
        {
            _le1.Dispose();
            _le2.Dispose();
        }
    }

    [TestFixture]
    public class LeaderElectionEventSpec
    {
        MonarchicalLeaderElection _le1;
        MonarchicalLeaderElection _le2;
        MonarchicalLeaderElection _le3;

        string _le1_pick;
        string _le2_pick;

        [SetUp]
        public void Given()
        {
            _le1 = new MonarchicalLeaderElection("10.10.10.1,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });
            _le2 = new MonarchicalLeaderElection("10.10.10.2,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });
            _le3 = new MonarchicalLeaderElection("10.10.10.3,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });

            _le1.Leader += peer => _le1_pick = peer;
            _le2.Leader += peer => _le2_pick = peer;

            Thread.Sleep(1000);
        }

        [Test]
        public void Test()
        {
            _le3.Dispose();
            Thread.Sleep(3000);

            Assert.That(_le1_pick, Is.EqualTo("10.10.10.2,2555"));
            Assert.That(_le2_pick, Is.EqualTo("10.10.10.2,2555"));
        }

        [TearDown]
        public void Finally()
        {
            _le1.Dispose();
            _le2.Dispose();
        }
    }

    [TestFixture]
    public class LeaderElectionSpec
    {
        MonarchicalLeaderElection _le1;
        MonarchicalLeaderElection _le2;
        MonarchicalLeaderElection _le3;

        [SetUp]
        public void Given()
        {
            _le1 = new MonarchicalLeaderElection("10.10.10.1,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });
            _le2 = new MonarchicalLeaderElection("10.10.10.2,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });
            _le3 = new MonarchicalLeaderElection("10.10.10.3,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });

            Thread.Sleep(1000);
        }

        [Test]
        public void Test()
        {
            Assert.That(_le1.CurrentLeader, Is.EqualTo("10.10.10.3,2555"));
            Assert.That(_le2.CurrentLeader, Is.EqualTo("10.10.10.3,2555"));
            Assert.That(_le3.CurrentLeader, Is.EqualTo("10.10.10.3,2555"));

            _le3.Dispose();
            Thread.Sleep(3000);

            Assert.That(_le1.CurrentLeader, Is.EqualTo("10.10.10.2,2555"));
            Assert.That(_le2.CurrentLeader, Is.EqualTo("10.10.10.2,2555"));
        }

        [TearDown]
        public void Finally()
        {
            _le1.Dispose();
            _le2.Dispose();
        }
    }
}