﻿using System;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Specs
{
    [TestFixture]
    public class TransportMessageSerializerSampler
    {
        [Test]
        public void SimpleTest()
        {
            var slr = new TransportMessageSerializer(){};

            var actual = slr.ToString(new TransportMessage()
            {
                Id = "1",
                CorrelationId = "2",
                From = "10.10.10.2,2555",
                To = "10.10.10.2,2555",
                Payload = "alive",
            });

            Console.WriteLine(actual);
        }

        [Test]
        public void PreserveIdTest()
        {
            var slr = new TransportMessageSerializer() { };

            var orig = new TransportMessage()
            {
                Id = "1",
                CorrelationId = "1",
                From = "10.10.10.2,2555",
                To = "10.10.10.2,2555",
                Payload = "alive",
            };

            var clone = slr.Clone(orig);

            Console.WriteLine(slr.ToString(orig));
            Console.WriteLine(slr.ToString(clone));

            Assert.AreEqual(orig.Id, "1");
            Assert.AreEqual(clone.Id, "1");
        }

        [Test]
        public void WhenNoIdProvidedCreatesOne()
        {
            var slr = new TransportMessageSerializer() { };

            var orig = new TransportMessage()
            {
                From = "10.10.10.2,2555",
                To = "10.10.10.2,2555",
                Payload = "alive",
            };

            var clone = slr.Clone(orig);

            Console.WriteLine(slr.ToString(orig));
            Console.WriteLine(slr.ToString(clone));

            Assert.IsNotNullOrEmpty(orig.Id);
            Assert.IsNotNullOrEmpty(clone.Id);

            Assert.AreEqual(clone.Id, orig.Id);
        }

        [Test]
        public void CloneWillChangeButNotMutate()
        {
            var orig = new TransportMessage()
            {
                From = "10.10.10.2,2555",
                To = "10.10.10.2,2555",
                Payload = "alive",
            };

            var clone = orig.Clone(message => { message.To = "dev/null"; });

            Assert.AreEqual(orig.To, "10.10.10.2,2555");
            Assert.AreEqual(clone.To, "dev/null");
        }
    }
}