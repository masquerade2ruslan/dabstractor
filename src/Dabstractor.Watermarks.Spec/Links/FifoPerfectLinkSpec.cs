﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Watermarks.Spec.Links
{
    [TestFixture]
    public class FifoPerfectLinkSpec
    {
        [Test]
        public void Send10KMessagesTest()
        {
            Stopwatch watch;

            const int tenK = 10000;

            using (var q = new FifoPerfectLink("10.10.10.2,2555"))
            using (var p = new FifoPerfectLink("10.10.10.1,2555"))
            {
                var meesagesReceived = new List<int>();

                q.Deliver += message => meesagesReceived.Add(1);

                watch = Stopwatch.StartNew();

                for (int i = 0; i < tenK; i++)
                    p.Send(new TransportMessage("10.10.10.2,2555", "alive" + i));

                while (meesagesReceived.Count < tenK)
                    Thread.Sleep(100);

                watch.Stop();
            }

            Console.WriteLine();
            Console.WriteLine("Messages             : " + tenK);
            Console.WriteLine("Duraton              : " + TimeSpan.FromMilliseconds(watch.ElapsedMilliseconds));
            Console.WriteLine("Roundtrip(millisec)  : " + (((float)watch.ElapsedMilliseconds / tenK)));

            Assert.Less(watch.ElapsedMilliseconds, 3500);
        }
    }
}