using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Dabstractor.P2pLinks;
using NUnit.Framework;

namespace Dabstractor.Watermarks.Spec.Links
{
    [TestFixture]
    public class FairLossLink2Spec
    {
        const int TenK = 10000;

        [Test]
        public void Send10KMessagesTest()
        {
            var meesagesReceived = new List<int>();

            Stopwatch watch;

            using (var p = new FairLossLink("10.10.10.1,2555"))
            using (var q = new FairLossLink("10.10.10.2,2555"))
            {
                q.Deliver += message => meesagesReceived.Add(1);

                watch = Stopwatch.StartNew();

                for (int i = 0; i < TenK; i++)
                    p.Send(new TransportMessage("10.10.10.2,2555", "alive"));

                while (meesagesReceived.Count < TenK)
                    Thread.Sleep(100);

                watch.Stop();
                p.Dispose();
            }

            Console.WriteLine();
            Console.WriteLine("Messages             : " + TenK);
            Console.WriteLine("Duraton              : " + TimeSpan.FromMilliseconds(watch.ElapsedMilliseconds));
            Console.WriteLine("Roundtrip(millisec)  : " + (((float)watch.ElapsedMilliseconds / TenK)));

            Assert.Less(watch.ElapsedMilliseconds, 1600);
        }
    }
}