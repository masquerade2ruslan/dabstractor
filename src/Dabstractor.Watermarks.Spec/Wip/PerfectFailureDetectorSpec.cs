﻿using System.Collections.Generic;
using System.Threading;
using Dabstractor.FailureDetectors;
using NLog;
using NUnit.Framework;

namespace Dabstractor.Watermarks.Spec.Wip
{
    [TestFixture]
    public class PerfectFailureDetectorSpec
    {
        private static readonly Logger Logger = LogManager.GetLogger("tst");


        [Test]
        public void Start3nodesAndKillOne()
        {
            Logger.Info("tc start");

            var pfd1 = new PerfectFailureDetector("10.10.10.1,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });
            var pfd2 = new PerfectFailureDetector("10.10.10.2,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });
            var pfd3 = new PerfectFailureDetector("10.10.10.3,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });

            pfd1.Crash += peer => Logger.Info("d1 Suspecting {0}", peer);
            pfd2.Crash += peer => Logger.Info("d2 Suspecting {0}", peer);
            pfd3.Crash += peer => Logger.Info("d3 Suspecting {0}", peer);

            Thread.Sleep(9000);

            pfd3.Dispose();
            Logger.Info("dispose called");


            Thread.Sleep(5000);
        }

        [Test]
        public void Start2nodes()
        {
            Logger.Info("tc start");

            var pfd1 = new PerfectFailureDetector("10.10.10.1,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });
            var pfd2 = new PerfectFailureDetector("10.10.10.2,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });

            pfd1.Crash += peer => Logger.Info("d1 Suspecting {0}", peer);
            pfd2.Crash += peer => Logger.Info("d2 Suspecting {0}", peer);

            Thread.Sleep(12000);

            pfd1.Dispose();
            pfd2.Dispose();
        }
    }
}