﻿using System;
using System.Collections.Generic;
using Dabstractor.FailureDetectors;
using NLog;

namespace Dabstractor.Cli
{
    class Program
    {
        private static readonly Logger Logger = LogManager.GetLogger("tst");

        static void Main(string[] args)
        {
            var pfd1 = new PerfectFailureDetector("10.10.10.1,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });
            pfd1.Crash += peer => Logger.Info("d1 observed Crash {0}", peer);

            var pfd2 = new PerfectFailureDetector("10.10.10.2,2555", new List<string>() { "10.10.10.1,2555", "10.10.10.2,2555", "10.10.10.3,2555" });
            pfd2.Crash += peer => Logger.Info("d2 observed Crash {0}", peer);

            Console.ReadKey();
        }
    }
}
